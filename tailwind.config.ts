import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        "inter": ["Inter", "serif"]
      },
      fontSize: {
        // Headings
        "h1": ["54px", "1.222"],
        "h2": ["38px", "1.21"],
        "h3": ["28px", "1.21"],
        "h4": ["22px", "1.18"],
        "p": ["16px", "1.5"],
        // Headings mobile
        "m-h1": ["30px", "1.2"],
        "m-h2": ["26px", "1.21"],
        "m-h3": ["22px", "1.21"],
        "m-h4": ["18px", "1.18"],
        "m-p": ["14px", "1.5"],
        // Big text
        "big": ["18px", "1.44"],
        "m-big": ["16px", "1.44"],
        // Button text
        "btn": ["0.75rem", "1.17"],
        "sm": ["0.875rem", "1.375rem"],
        "xs": ["0.75rem", "0.875rem"],
        // Caption text
        "caption": ["14px", "1.57"],
        "m-caption": ["12px", "1.2"],
        // Custom
        "vacancy-details-h3": ["32px", "1.21"],
        "vacancy-details-big": ["16px", "1.44"],
      },
      colors: {
        primary: "#0062D8",
        secondary: "#0255bb",
        red: "#E41616",
        dark: "#020F18",
        concrete: "#f3f3f3",
        "light-gray": "#E4E4E4",
        silver: "#B0B5B9",
        "line-light-gray": "#D4D4D4",
        "primary-light-gray": "#B3B8BD",
        "secondary-light-gray": "#9D9D9D",
        "background-light-gray": "#F6F6F6",
        "section-light-gray": "#F7F7F9",
        "seo-primary": "#4D4D4D",
        "seo-secondary": "#4C4C4C",
        "file-primary-gray": "#878787",
        overlay: "#0000004D"
      },
      backgroundImage: {
        "chevron-down": "url('../../public/icons/chevron-down.svg')",
      },
      backgroundPosition: {
        "right-center": "105% center"
      },
    },
  },
  plugins: [],
};
export default config;
