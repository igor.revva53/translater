import { NextRequest, NextResponse } from 'next/server';
import * as deepl from 'deepl-node';

export async function POST(request: NextRequest) {
  const { htmlInput, targetLanguage } = await request.json();
  const authKey: string | undefined = process.env.DEEPL_API_KEY;

  if (!authKey) {
    return NextResponse.json({ error: 'DEEPL_API_KEY is not set' }, { status: 500 });
  }

  try {
    const translator = new deepl.Translator(authKey);
    const result = await translator.translateText(htmlInput, null, targetLanguage);
    let translatedText: string;

    if (Array.isArray(result)) {
      translatedText = result.map(r => r.text).join(' ');
    } else {
      translatedText = result.text;
    }

    return NextResponse.json({ translatedText });
  } catch (error) {
    return NextResponse.json({ error: 'Error translating HTML1' }, { status: 500 });
  }
}
