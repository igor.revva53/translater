import {TranslateText} from "@/components";

export default function Home() {
  return (
    <main className="container mx-auto px-4">
      <TranslateText />
    </main>
  );
}
