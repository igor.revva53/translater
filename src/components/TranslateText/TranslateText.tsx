'use client'

import {useEffect, useState} from 'react';
import axios from "axios";
import parse from "html-react-parser";
import Editor from '@monaco-editor/react';

import {Spinner} from "@/components";

const languages = [
  { code: 'EN-US', name: 'English (American)' },
  { code: 'EN-GB', name: 'English (British)' },
  { code: 'ES', name: 'Spanish' },
  { code: 'FR', name: 'French' },
  { code: 'RU', name: 'Russian' },
  { code: 'UK', name: 'Ukrainian' },
];

const TranslateText = () => {
  const [htmlInput, setHtmlInput] = useState<string>('');
  const [translatedHtml, setTranslatedHtml] = useState<string>('');
  const [language, setLanguage] = useState<string>('EN-US');
  const [isValid, setIsValid] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const handleTranslate = async () => {
    try {
      setLoading(true);

      const response = await axios.post('/api/translate', {
        htmlInput,
        targetLanguage: language
      });

      setTranslatedHtml(response.data.translatedText);
    } catch (error) {
      setIsValid(false);
      setError("Something went wrong, try again");

      setTimeout(() => {
        setError("");
        setIsValid(true);
      }, 5000);
    } finally {
      setLoading(false);
    }
  };

  const validateHTML = (text: string): boolean => {
    const plainText = text.replace(/<[^>]*>/g, '').trim();

    return plainText.length > 0;
  };

  useEffect(() => {
      setIsValid(validateHTML(htmlInput));
  }, [htmlInput]);

  return (
    <section className="py-4 w-full">
      <h1 className="text-2xl text-center font-inter font-bold mb-4">
        HTML Translator
      </h1>

      <Editor
        className="w-full h-full aspect-[1200/480] mb-4"
        defaultLanguage="html"
        theme="vs-dark"
        defaultValue={htmlInput}
        onChange={(value) => setHtmlInput(value || '')}
        loading={
          <div className="w-full h-full aspect-[1200/480] flex justify-center items-center">
            <Spinner />
          </div>
        }
      />

      <div className="flex items-center mb-4 justify-center">
        <label
          className="font-inter mr-2.5"
          htmlFor="language-select"
        >
          Select Language:
        </label>

        <select
          className="border py-[10px] pl-[10px] pr-[40px] text-sm appearance-none bg-chevron-down bg-[length:20px_15px] bg-[95%_50%] bg-no-repeat"
          id="language-select"
          value={language}
          onChange={(e) => setLanguage(e.target.value)}
        >
          {languages.map((lang) => (
            <option key={lang.code} value={lang.code}>
              {lang.name}
            </option>
          ))}
        </select>
      </div>

      <button
        className="button button-primary button-small mx-auto"
        onClick={handleTranslate}
        disabled={!isValid || loading}
      >
        Translate
      </button>

      {error && (
        <p className="text-red text-center mt-4">
          {error}
        </p>
      )}

      {translatedHtml && (
        <>
          <h2 className="text-2xl text-center font-inter font-bold mt-8 mb-4">
            Translated HTML
          </h2>

          <div>
            {parse(translatedHtml)}
          </div>
        </>
      )}
    </section>
  );
};

export default TranslateText;